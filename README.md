# Overview


# Installation

These installation instructions work for a mac with [Homebrew](https://brew.sh/) installed.

1. Client-side

    1.1 kubectl
  
    [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-linux) is a client-side, command line tool (binary executable) that can connect to a [Kubernetes](https://kubernetes.io/) cluster to manage Kubernetes resources.

    `brew install kubectl`
    Test with `kubectl version --client`

    see also:
    * [Cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)


    1.2. Kamel

    [Kamel](https://github.com/apache/camel-k/) (camel-k-client) is the command-line tool to configure and run Camel integrations with Camel K on a Kubernetes cluster. To use it, you need to be connected to a Kubernetes cluster.

    To install, download and uncompress the archive. Put the small binary file named `kamel` into your system path (e.g. /usr/local/bin).

    Then, log into your cluster using your standard Kubernetes client tool (e.g. `oc` or `kubectl`) and execute `kamel install`. This will configure the cluster with the Camel K custom resource definitions an install the operator on the current namespace. Custom Resource Definitions (CRD) are cluster-wide objects and need admin rights for installation.


2. Server-side

    2.1. Precondition: Hypervisor

    Minikube needs a [Hypervisor](https://en.wikipedia.org/wiki/Hypervisor). A hypervisor (or virtual machine monitor VMM) is a software that creates and runs virtual machines (guests) on a host machine. One of the following products can be used:

    * [Hyperkit](https://github.com/moby/hyperkit)
    * [VirtualBox](https://www.virtualbox.org)
    * [VMware Fusion](https://www.vmware.com/products/fusion)). 


    We install Hyperkit here with the following command:

    * `brew install hyperkit`        # VM driver
    * to make hyperkit the default driver:   `minikube config set driver hyperkit`


  2.2. Minikube

  [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/) builds a [Kubernetes](https://kubernetes.io/) cluster in a single host with a set of small resources to run a small Kubernetes deployment. It is meant for testing scenarios in the local environment.

  The current version as of march 2020 is v1.8.1.

* install: `brew install minikube`
* upgrade: `brew upgrade minikube`
* `cd /usr/local/bin; ln -s ../Cellar/minikube/1.8.1/bin/minikube`
* start:  `minikube start --vm-driver hyperkit`
* enable the registry: `minikube addons enable registry`
* configure more RAM: `minikube config set memory 4096`
* start the minikube [dashboard](http://127.0.0.1:65226/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/#/overview?namespace=default): `minikube dashboard`
* verify installation:  `minikube status`
* stop minikube:  `minikube stop`

  see also:
* [Service Exposure](https://medium.com/@pczarkowski/kubernetes-services-exposed-86d45c994521)

  2.3. Camel K

  [Camel](https://camel.apache.org) is an open source integration framework built in Java. It is based on Enterprise Integration Patterns (EIP) and contains several hundred components to access databases, message queues, APIs etc. It supports about 50 data formats and can translate messages in multiple formats.

  [Camel K](https://camel.apache.org/camel-k/latest/) is a lightweight integration framework built from Apache Camel that runs natively on [Kubernetes](https://kubernetes.io/) and is specifically designed for serverless and microservices architectures. Camel K can run on any Kubernetes cluster such as:

  * [Minikube](https://camel.apache.org/camel-k/latest/installation/minikube.html) (see installation instructions above)
  * [Minishift](https://camel.apache.org/camel-k/latest/installation/minishift.html)
  * [Google Kubernetes Engine (GKE)](https://camel.apache.org/camel-k/latest/installation/gke.html)
  * [OpenShift](https://camel.apache.org/camel-k/latest/installation/openshift.html)

  Camel K is an operator written in Go. This converts the Camel custom resource into a Kubernetes pod.

  Camel K supports different runtimes (Spring-Boot, Karaf, EAP, standalone). Integration files can be written in different programming languages: Java, Groovy, Kotlin, JS, XML.

  see also:
  * [github](http://github.com/apache/camel-k)
  * [blog entry](https://www.nicolaferraro.me/2018/10/15/introducing-camel-k/)  from Nicola Ferraro, 15.10.2018
  * [video](https://youtu.be/dkOaf2YSqOw)

  Installation:
  * login to Kubernetes
  * `kamel install`    # Camel K is installed in namespace default
  * `kamel run --dev FILE`  # run an integration
  * `kamel run --context=quarked --dev FILE`   # run java camel routes with quarkus
  * `kamel get`      # monitor status
  * reinstall Camel K:  `kubectl delete integrationplatform camel-k; kubectl delete ictx jvm groovy kotlin; kamel install`
  * uninstall:  `kubectl delete all,pvc,configmap,rolebindings,clusterrolebindings,secrets,sa,roles,clusterroles,crd -l 'app=camel-k‘`
  * re-install: `kamel reset; kamel install`


3. Groovy DSL

  * https://cwiki.apache.org/confluence/display/CAMEL/Groovy+DSL
  * https://camel.apache.org/camel-k/latest/languages/groovy.html

4. Examples  

  This Readme just gives an overview about the different camelk tests. Please see the detailed instructions about build, deployment
  and testing in the specific source code file.

  4.1. helloworld

  * name: 01_helloworld.groovy
  * functionality: Every 3 seconds, this test example writes a hello-message into the log. Use this to verify that your installation was successful.
  * status: tested

  4.2. file-test

  * name: 02_file-test.groovy
  * functionality: reads a local file and writes it into another directory
  * status: tested

   4.3. rest-consumer-test

  * name: 03_rest-consumer-test.groovy
  * functionality: tests for handling the rest requests from a client (consumer mode)
  * status: tested

   4.4. rest-producer-test

  * name: 04_rest-producer-test.groovy
  * functionality: tests for calling externel rest apis (producer mode)
  * status: not implemented yet

  4.5. validator-test

  * name: 05_validator-test.groovy
  * functionality: validates a XML file with a XSD schema
  * status: not yet implemented

  4.6. xslt-test

  * name: 06_xslt-test.groovy
  * functionality: XSLT-conversion of a XML file into another format using XSLT template
  * status: not yet implemented

  4.7. fop-test

  * name: 07_fop-test.groovy
  * functionality: converts a xml-file into a pdf.
  * status: not yet implemented
 
  4.8. sftp-test

  * name: 08_sftp-test.groovy
  * functionality: downloads and uploads a file from/to a ftp server.
  * status: not yet implemented

  4.9. aix

  * name: aix.groovy
  * functionality: accepts parameters dataUri, templateUri, resultUri and produces pdfs (this is the real fop service). 
  * run with: `kamel run --dev -d camel-servlet -d camel-jackson -d camel-rest aix.groovy`
  * test with: `curl --data 'dataUri=www.arbalo.ch/data.xml&templateUri=www.arbalo.ch/template.xslt&resultUri=sftp://www.arbalo.ch/outbox/result.pdf' http://localhost:8080`
  * test with: `echo '{"dataUri": "sftp://localhost/data/data.xml", "templateUri": "sftp://localhost/templates/template.xslt", "resultUri": "sftp://localhost/output/result.pdf" }' | curl -d @- http://localhost/8080/delivery`
  * status: not yet implemented


# Documentation

* [Camel Website](https://camel.apache.org)
* [Camel User Manual](https://camel.apache.org/manual/latest/index.html)
* [Camel Component Reference](https://camel.apache.org/components/latest/index.html)
* [Camel Examples](https://github.com/apache/camel-examples/tree/master/examples#welcome-to-the-apache-camel-examples)
* [Minikube Installation Instructions](https://kubernetes.io/docs/tasks/tools/install-minikube/)

