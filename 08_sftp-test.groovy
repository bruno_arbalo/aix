/*
  Camel-K / language: groovy
  License: MIT
  Copyright: 2020 Arbalo AG

  preconditions: 
  A running sftp server is required for this example. You may use the macOS embedded sftp server like this:
    * in system preferences, open 'Remote Login' for Administrators (ssh, sftp)
    * test with `sftp localhost`

  run it with:
  kamel run --dev --name sftp-test -dependency=camel-ftp 08_sftp-test.groovy

  to test:
  tbd
*/


from('sftp://sftptestuser:!vryScurPwd4me:@localhost:22/test/test.txt')
  .to('log:info')
  .to('sftp://sftptestuser:!vryScurPwd4me@localhost:22/result');


