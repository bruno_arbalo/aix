/*
  Camel-K / language: groovy
  License: MIT
  Copyright: 2020 Arbalo AG

  preconditions:
    add persistent volume
      kubectl apply -f pv-aix.yaml
      kubectl get pv pv-aix        -> should be AVAILABLE
      kubectl apply -f pvc-aix.yaml
      kubectl get pv pv-aix       -> should be BOUND with claim=default/pvc-aix


  run it with:
  kamel run --dev 
    --name aix 
    --dependency camel-undertow
    --property camel.rest.port=8080
    -v pvc-aix:/data
    --logging-level org.apache.camel.k=DEBUG
    --open-api aix.json
    aix.groovy

  to test:
  - make the service externally available: 
    kubectl expose deployment aix --port=8080 --type=NodePort --name aix-ext
    NAME               TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
    ext-consumer-test  NodePort    10.104.246.237   <none>        8080:30772/TCP   8s
  - get the access url
    minikube service --url aix-ext
    http://192.168.64.3:30772
  - call the service
    curl -X POST -H "Content-Type: application/json" -d @aix-test.json http://192.168.64.3:30772/v1/delivery
*/

/* camel {
    components {
        camel-undertow {
            host = localhost
            port = 8080
            bindingMode = RestBindingMode.auto
        }
    }
} */

/* rest("/v1").description("Deliver invoice data to Arbalo Invoice Exchange AIX.")
  .consumes("application/json")
  .produces("application/json")
  .post("/delivery")
  .to("direct:createInvoice") */

// doing something with errors - with no extra configuration this will push it to console
onException(Exception.class).to("log:error") 


from("direct:createInvoice")
    .to("xj:file:///usr/share/test-data/seeclub/xslt/convert-xj.xsl?transformDirection=JSON2XML&lazyStartProducer=true")
    .to("log:api?showAll=true&multiline=true")
    .setBody()
        .simple("Delivery succeeded.")
