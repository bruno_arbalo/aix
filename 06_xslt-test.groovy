/*
  Camel-K / language: groovy
  License: MIT
  Copyright: 2020 Arbalo AG

  run it with:
  kamel run --name xslt-test --dev 06_xslt-test.groovy

  to test:
  tbd
 */


from("file:seeclub/source/seeclub-invoice-test.xml").
  to("xslt:file:///Users/bruno/proj/bk-services/sv-fop/seeclub/xslt/yearlyMembershipFee.xsl").
  to("file:target/xml");
