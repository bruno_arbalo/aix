/*
  Camel-K / language: groovy
  License: MIT
  Copyright: 2020 Arbalo AG
  
  run it with:   
  kamel run --name file-test --dev 02_file-test.groovy

  to test:
  /deployments is the only directory which is accessible for non-root

  copy a local file into the container
    kubectl cp test-data/inbox/test.txt default/file-test-HASH:/deployments/inbox/test.txt

  if all goes well, this file should immediately be copied into /deployments/outbox. You may test this using the K8s dashboard
    * `exec on pod file-test`
    * `ls outbox`

*/

from("file:/deployments/inbox?noop=true")
.to("file:/deployments/outbox");