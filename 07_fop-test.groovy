/*
  Camel-K / language: groovy
  License: MIT
  Copyright: 2020 Arbalo AG
  
  run it with:   
  kamel run --name fop-test --dev 07_fop-test.groovy

  to test:
  tbd
*/
from("file:/deployments/inbox?noop=true")
    .to("xslt:/deployments/template/template.xsl")
    .to("fop:application/pdf")
    .to("file:/deployments/outbox");