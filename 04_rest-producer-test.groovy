/*
  Camel-K / language: groovy
  License: MIT

  Consumer:  Camel is exposing or providing a REST service
  Producer: Camel is invoking an external REST service

  This CONSUMER service exposes a very easy REST API 'say'.
  It answers a GET call to /say/hello with 'Hello !'.
  It answers a GET call to /say/bye with 'Bye Bye !'.
  It answers a POST call to /say/saying with 'Hello, saying: [your body data]'.

  run it with:
  kamel run --dev 
    --name rest-consumer-test 
    --dependency camel-undertow
    --property camel.rest.port=8080
    --logging-level org.apache.camel.k=DEBUG
    03_rest-consumer-test.groovy

  setting the logging level to DEBUG, you can see the print of the rest routes

  to monitor with Prometheus:   -t prometheus.enabled=true
  to monitor with hawtio:   -t jolokia.enabled=true

  To test:
  - make the service externally available: 
    kubectl expose deployment rest-consumer-test --port=8080 --type=NodePort --name ext-consumer-test
    NAME               TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
    ext-consumer-test  NodePort    10.104.246.237   <none>        8080:30772/TCP   8s
  - get the access url
    minikube service --url ext-consumer-test
    http://192.168.64.3:30772
  - call the service
    curl http://192.168.64.3:30772/say/hello
    Hello !

  (Build and deploy an API with camel-k on openshift)[https://developers.redhat.com/blog/2019/04/25/build-and-deploy-an-api-with-camel-k-on-red-hat-openshift/]
*/

rest("/say")
  .get("/hello").to("direct:hello")
  .get("/bye").to("direct:bye")
  .get("/{myname}").to("direct:myname")
  .post().consumes("application/json").to("direct:saying")

from("direct:hello")
  .log("get on /say/hello received.")
  .transform().constant("Hello !")

from("direct:bye")
  .log("get on /say/bye received.")
  .transform().constant("Bye Bye !")

from("direct:myname")
  .log("get on /myname/${header.myname} received.")
  .transform().simple("Hi ${header.myname}")

from("direct:saying")
  .log("post on /say received.")
  .log("body=${param1}")
  .transform().constant("Hello, saying")

                