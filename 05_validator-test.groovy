/*
  Camel-K / language: groovy
  License: MIT
  Copyright: 2020 Arbalo AG

  run it with:   
  kamel run --name validator-test --dev 05_validator-test.groovy  

  to test:
  tbd
*/

from("file:/deployments/inbox")
    .to("validator:/deployments/schema/seeclub-addresses.xsd")
    .to("log:info");