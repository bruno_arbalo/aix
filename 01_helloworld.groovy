/*
  Camel-K / language: groovy
  License: MIT
  Copyright: 2020 Arbalo AG

  run it with:
  kamel run --name helloworld --dev 01_helloworld.groovy

  to test:
  check the logs of the helloworld pod. You should see Hello world entries.
*/
from('timer:tick?period=3s')
  .setBody().constant('Hello world from Camel K')
  .to('log:info')
